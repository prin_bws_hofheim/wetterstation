/*
 * demo1_main.c
 *
 * Created: 02.06.2021 17:07:56
 * Author : Thomas
 * 
 *
 * 0x05 led: turn LED on
 * PC (request): 0x05
 *
 * 0x06 led: rurn LED off
 * PC (request): 0x06 
 */

#include <avr/io.h>

//log
unsigned char recLog[100];
unsigned char recLogCounter;

void init() {
    //init UART
    UBRR0H = 0x00;
    UBRR0L = 0x67;
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);
    UCSR0C = (1<<USBS0)|(3<<UCSZ00);

    //init LED
    DDRB |= 0x20;
    PORTB |= 0x20;

    // init log
    for (int recLogCounter = 0; recLogCounter < 100; recLogCounter++)
        recLog[recLogCounter] = 0x00;
    recLogCounter = 0x00;
}

void reqSendByButton() {
    while (!(UCSR0A & (1 << UDRE0))) {
    }
    UDR0 = 0x42;
}

void reqLedToggle(unsigned char recValue) {
    if (0x35 == recValue)
        PORTB &= 0xDF;
    else
        PORTB |= 0x20;
}

void reqLedOn() {
    PORTB |= 0x20;
}

void reqLedOff() {
    PORTB &= 0xDF;
}

int main(void) {
    unsigned char inByte;
    int requestStage = 0x00;

    init();

    //main loop  
    while (1) {
        while (!(UCSR0A & 0x80)) {
            if (PINB & 0x80)
                reqSendByButton();
        }
        while (!(UCSR0A & 0x20))
            ;
        inByte = UDR0;
        recLog[recLogCounter] = inByte;
        recLogCounter++;
        if ((0x00 == requestStage)
                && (0x05 == inByte || 0x06 == inByte || 0x07 == inByte || 0x08 == inByte || 0x35 == inByte || 0x36 == inByte || 0x37 == inByte || 0x38 == inByte)) {
            switch (inByte) {
                case 0x05:
                case 0x35:
                    reqLedOn();
                    requestStage = 0x00;
                    break;

                case 0x06:
                case 0x36:
                    reqLedOff();
                    requestStage = 0x00;
                    break;

                case 0x07:
                case 0x37:
                    for (int i = 0; i < recLogCounter; i++) {
                        while (!(UCSR0A & (1 << UDRE0))) {
                        }
                        UDR0 = recLog[i];
                    }
                    break;

                case 0x08:
                case 0x38:
                    for (int i = 0; i < recLogCounter; i++) {
                        recLog[i] = 0x00;
                        recLogCounter = 0x00;
                    }
                    break;

                default:
                {
                }
            }
        } else {
            if ((0x00 == requestStage)
                    && (0x04 == inByte || 0x34 == inByte)) {
                requestStage = inByte;
            } else {
                if (0x00 != inByte) {
                    switch (requestStage) {
                        case 0x04:
                        case 0x34:
                            reqLedToggle(inByte);
                            requestStage = 0x00;
                            break;

                        default:
                        {
                        }
                    }
                }
            }
        }
    }
}

