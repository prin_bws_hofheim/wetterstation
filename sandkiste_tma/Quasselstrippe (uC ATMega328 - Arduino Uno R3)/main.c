/*
* quasselstrippe3.c
*
* Created: 11.09.2021 15:37:17
* Author : Thomas
*/

#include <avr/io.h>
#include "quasselstr_defines_globalVars.h"
#include "quasselstrippe_bin.c"
#include "quasselstrippe_ascii.c"


void init()
{
	//init UART
	UBRR0H = 0x00;
	UBRR0L = 0x67;
	UCSR0B = 0x18;
	UCSR0C = 0x06;

	DDRB |= 0x20;
	PORTB |= 0x20;
	
	for(int recLogCounter = 0; recLogCounter < arraySize; recLogCounter++)
	{
		recLog[recLogCounter] = 0x00;
		req01Data[recLogCounter] = 0x00;
	}
	recLogCounter = 0x00;
	req01Counter = 0x00;
	req01SizeIn = 0x00;
}

int main(void)
{
	char swichOn = 0x00;
	unsigned char inByte = 0x00;
	unsigned char binAsciiSwitch = 0x00; // 0x00 = bin, 30 = Ascii
	
	init();

	while (1)
	{
		while(!(UCSR0A & 0x80))
		{
			if(~PINB & 0x80)
			{
				if(!swichOn)
				{
					if(PORTB & 0x20)
					PORTB &=0xDF;
					else
					PORTB |=0x20;
					
					UDR0 = 0x33;
					swichOn = 1;
				}
			}
			else
			swichOn = 0;
		}
		while (!(UCSR0A & 0x20))
		;
		inByte = UDR0;
		binAsciiSwitch = inByte;
		recLog[recLogCounter] = inByte;
		recLogCounter++;

		switch(binAsciiSwitch)
		{
			case 0x00:
			processProtocollBin();
			break;
			
			case 0x30:
			processProtocollAscii();
			break;
		}
	}
}

