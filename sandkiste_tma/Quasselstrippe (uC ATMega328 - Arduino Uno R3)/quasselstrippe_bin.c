/*
* quasselstrippe_bin.c
*
* Created: 11.09.2021
* Author : Thomas
*
* Bin�r Version des Protokolls
*
* 0x01 echo: bekommt string, sendet ihn 1:1 zur�ck. (noch nicht implementiert!)
* 0x02 add: bekommt eine Zahl (1 Byte) inc um 1, sendet ihn zur�ck.
* 0x03 btn: bei Bet�tigen des Tasters wird der letzte Wert der Zahl aus 0x02 geschickt (ohne inc)
* 0x04 led: schaltet die LED (Val '5' (0x35) ein, sonst aus)
*
* 0x01 echo: bekommt string, sendet ihn 1:1 zur�ck.
* PC (Auftrag): 0x01 0xnn 0x... <nn Bytes, max 31>
* uC (Antwort): 0x01 0xnn 0x... <nn Bytes, max 31>
* PC sendet "Helo": 0x01 0x04 0x48 0x65 0x6C 0x6F
* uC Antwort: 0x01 0x04 0x48 0x65 0x6C 0x6F
*
* 0x02 add: bekommt eine Zahl (1 Byte) inc um 1, sendet ihn zur�ck.
* PC (Auftrag): 0x02 0xnn
* uC (Antwort): 0x02 0xnn+1
* PC sendet  "41": 0x02 0x29
* uC Antwort "42": 0x01 0x2A
*
* 0x03 btn: bei Bet�tigen des Tasters wird der letzte Wert der Zahl aus 0x02 geschickt (ohne inc)
* uC (Meldung): 0x03 0xnn (Startwert: 0x00)
* uC hat als letzten Wert 0x29 empfangen, 0x2A gesendet
* uC sendet 0x03 0x2B
*
* 0x04 led: schaltet die LED (Val '5' (0x35) ein, sonst aus)
* PC (Auftrag): 0x04 0xnn (n = 0x35(ASCII '5') ein, sonst aus)
*
* 0x05 led: schaltet die LED ein
* PC (Auftrag): 0x05
*
* 0x06 led: schaltet die LED aus
* PC (Auftrag): 0x06
*/

#include "quasselstr_defines_globalVars.h"

char reqEchoRecBin(unsigned char inByte);
char reqEchoSendBin();
void reqAddBin(unsigned char recValue);
void reqBtnBin(unsigned char recValue);
void reqLedToggleBin(unsigned char recValue);
void reqLedOnBin();
void reqLedOffBin();
void reqSendLogBin();
void reqClearLogBin();



char reqEchoRecBin(unsigned char inByte)
{
	char retValue= 0xff;
	if(req01Counter < arraySize-1)
	req01Data[req01Counter] = inByte;
	req01Counter++;
	
	if(req01Counter >= req01SizeIn)
	{
		retValue = reqEchoSendBin();
	}
	return retValue;
}

char reqEchoSendBin()
{
	for(int i = 0; i < req01SizeIn; i++)
	{
		while(!(UCSR0A & (1<<UDRE0)))
		{
		}
		UDR0 = req01Data[i];
	}
	req01SizeIn = 0x00;
	req01Counter = 0x00;
	return 0x00;
}

void reqAddBin(unsigned char recValue)
{
	recValue++;
	while (!(UCSR0A & 0x20))
	;
	UDR0 = recValue;
}
void reqBtnBin(unsigned char recValue)
{
	while (!(UCSR0A & 0x20))
	;
	UDR0 = recValue;
}
void reqLedToggleBin(unsigned char recValue)
{
	if(0x35==recValue)
	PORTB &=0xDF;
	else
	PORTB |= 0x20;
}

void reqLedOnBin()
{
	PORTB |= 0x20;
}

void reqLedOffBin()
{
	PORTB &=0xDF;
}

void reqSendLogBin()
{
	for(int i = 0; i < recLogCounter; i++)
	{
		while(!(UCSR0A & (1<<UDRE0)))
		{
		}
		UDR0 = recLog[i];
	}
}

void reqClearLogBin()
{
	for(int i = 0; i < recLogCounter; i++)
	{
		recLog[i] = 0x00;
		recLogCounter = 0x00;
	}
}

void processProtocollBin()
{
	int requestStage = 0x00;
	char swichOn = 0x00;
	unsigned char inByte = 0x00;

	while (1)
	{
		while(!(UCSR0A & 0x80))
		{
			if(~PINB & 0x80)
			{
				if(!swichOn)
				{
					if(PORTB & 0x20)
					PORTB &=0xDF;
					else
					PORTB |=0x20;
					
					UDR0 = 0x03;
					swichOn = 1;
				}
			}
			else
			swichOn = 0;
		}
		while (!(UCSR0A & 0x20))
		;
		inByte = UDR0;
		//    UDR0 = inByte; // echo
		recLog[recLogCounter] = inByte;
		recLogCounter++;
		if((0x00 == requestStage)
		&& (0x03 == inByte || 0x05 == inByte || 0x06 == inByte || 0x07 == inByte  || 0x08 == inByte))
		{
			switch(inByte)
			{
				case 0x03:
				reqBtnBin(inByte);
				requestStage = 0x00;
				break;
				
				case 0x05:
				reqLedOnBin();
				requestStage = 0x00;
				break;
				
				case 0x06:
				reqLedOffBin();
				requestStage = 0x00;
				break;
				
				case 0x07:
				reqSendLogBin();
				break;

				case 0x08:
				reqClearLogBin();
				break;
				
				default:
				{
				}
			}
		}
		else
		{
			if((0x00 == requestStage)
			&& (0x01 == inByte || 0x02 == inByte || 0x04 == inByte))
			{
				requestStage = inByte;
			}
			else
			{
				switch(requestStage)
				{
					case 0x01:
					req01SizeIn = inByte;
					requestStage = 0x11;
					break;

					case 0x02:
					reqAddBin(inByte);
					requestStage = 0x00;
					break;
					
					case 0x04:
					reqLedToggleBin(inByte);
					requestStage = 0x00;
					break;
					
					case 0x11:
					if(0x00 == reqEchoRecBin(inByte))
					requestStage = 0x00;
					break;
					
					default:
					{
					}
				}
			}
		}
	}
}
