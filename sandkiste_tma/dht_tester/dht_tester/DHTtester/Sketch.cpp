﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include <DHT.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
//Beginning of Auto generated function prototypes by Atmel Studio
//End of Auto generated function prototypes by Atmel Studio



const int HYT939_ADDR = 0x28;
const int LED_PIN = 13;
  //Skalierungsfaktoren laut Datenblatt
const double TFACTOR = 99.2909;
const double TDELTA  = 40.0;
const double HFACTOR = 163.83;
const int chipSelect = 4;

#define DHTPIN 2     // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 3 (on the right) of the sensor to GROUND (if your sensor has 3 pins)
// Connect pin 4 (on the right) of the sensor to GROUND and leave the pin 3 EMPTY (if your sensor has 4 pins)
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  Wire.begin();

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
//  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C ;; "));
//  Serial.print(f);
//  Serial.print(F("°F  Heat index: "));
//  Serial.print(hic);
//  Serial.print(F("°C \n"));
//  Serial.print(hif);
//  Serial.println(F("°F"));



String dataString = "";
  unsigned int traw;
  unsigned int hraw;
    
  double temp;
  double hum;
  int i;
  unsigned char buffer[4];
  
 //Messung starten    
 Wire.beginTransmission(HYT939_ADDR); // transmit to device #40 (0x28)
 Wire.endTransmission();     // stop transmitting
  
  //10s warten
  delay(10000);
  
  //4 Bytes vom Sensor lesen
  Wire.requestFrom(HYT939_ADDR, 4,true);
  i=0;
  
while(Wire.available()) {
    char c = Wire.read();    // receive a byte as character
    buffer[i]=c;
    i++;
    }
  
  //Rohdaten aus Puffer lesen
  traw=buffer[2]*256+buffer[3];
  hraw=buffer[0]*256+buffer[1];
  
  //Daten laut Datenblatt maskieren
traw&=0xfffc;
hraw&=0x3fff;
traw>>=2;

//Rohdaten ausgeben, zur eventuellen Fehlersuche
//Serial.print("\r\nTemp Raw:");
//Serial.println(traw);
//Serial.print("Hum Raw:");
//Serial.println(hraw);


//Rohdaten Umrechnen
temp=(double)traw/TFACTOR;
temp=temp-TDELTA;
hum=(double)hraw/HFACTOR;

Serial.print("Temp:");
Serial.print(temp);
Serial.print(';');

Serial.print("Hum:");
Serial.println(hum);
}
