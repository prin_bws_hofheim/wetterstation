/*
 * scanner_dummy_1_atmega328.c
 *
 * Created: 09.07.2021 10:19:11
 * Author : thomas.maul
 */ 


/**  ********************************************************************/
/* Spezifikation Schnittstelle:                                         */
/* RS232 (ggf als USB-Kapselung)                                        */
/* 9600, 8, E, 1                                                        */
/* Baud: 9600                                                           */
/* Datenbits: 8                                                         */
/* Parit�t: even (gerade)                                               */
/* Stoppbits: 1                                                         */
/*                                                                      */
/* Protokoll PC-ATMega                                                  */
/* <Start><Befehl><Parameter(optional)>                                 */
/* Start  0xAA                                                          */
/* Befehl (PC->ATMega)                                                  */
/*   Scan 0x11                                                          */
/*   Test 0xBn (n=0..f, muss spezifiziert werden)                       */
/*        0xB1 falsche Pruefziffer (konstant 0)                         */
/*        0xB2 falsche Antwort (0xA0 statt 0x11)                        */
/*   Load Data (Scan and test)                                          */
/*     scan: 0xE1 Len (1 Byte), Data(n Byte)                            */
/*     test: 0xE2 Len (1 Byte), Data(n Byte)                            */
/*                                                                      */
/* Antworten (ATMega->PC; PC->ATMega)                                   */
/*   Scan 0x11 (Parameter: GTin1-Werte als UInt-Array (UChar))          */
/*   Err  0xFn (n=0..f, muss spezifiziert werden)                       */
/*   AKN  0xA0                                                          */
/*   NAK  0xAF                                                          */
/********************************************************************  **/

#include <avr/io.h>

#define recLogSize 5

unsigned char recLog[recLogSize];
unsigned char recLogCounter;

void init()
{
  //init UART
  UBRR0H = 0x00;
  UBRR0L = 0x67;
  UCSR0B = 0x18;
  UCSR0C = 0x06;

  DDRB |= 0x20;
  
  for(int recLogCounter = 0; recLogCounter < recLogSize; recLogCounter++)
  recLog[recLogCounter] = 0x00;
  recLogCounter = 0x00;  
}



void reqScan()
{
	unsigned char recValue = 0x00;
	
	while (!(UCSR0A & 0x20))
	;
	UDR0 = recValue;
	
}

void reqLedToggle(unsigned char recValue)
{
  if(0x35==recValue)
    PORTB &=0xDF;
  else
    PORTB |= 0x20;
}

void reqLedOn()
{
  PORTB |= 0x20;
}

void reqLedOff()
{
  PORTB &=0xDF;
}

void logIncomming(unsigned char inByte)
{
	if(recLogCounter < recLogSize-2)
	{
		recLog[recLogCounter] = inByte;
		recLogCounter++;
	}
	else
	{
		recLog[recLogSize-1] = 0xed;
		recLog[0x00] = 0xbe;
		recLogCounter = 0x01;
	}
}

int main(void)
{

  init();
  
//  enum requestType{requestNone = 0x00, requestEchoBin = 0x01, requestAddBin 0x02, requestBtnBin 0x03, requestLedToggleBin 0x04, requestLedOnBin 0x05, requestLedOffBin 0x06, requestSendRecBufferBin 0x07, requestClearRecBufferBin 0x08,
//                 requestEchoAscii = 0x31, requestAddAscii 0x32, requestBtnAscii 0x33, requestLedToggleAscii 0x34, requestLedOnAscii 0x35, requestLedOffAscii 0x36, requestSendRecBufferAscii 0x37, requestClearRecBufferAscii 0x08};
  unsigned char inByte;
//  enum requestType 
  int requestStage = 0x00;
  
  while (1) 
  {
    while(!(UCSR0A & 0x80))
    {
    }
    while (!(UCSR0A & 0x20))
      ;
    inByte = UDR0;
//    UDR0 = inByte; // echo
    logIncomming(inByte);
	
    if((0x00 == requestStage) 
       && (0x03 == inByte || 0x05 == inByte || 0x06 == inByte || 0x07 == inByte || 0x08 == inByte 
	    || 0x33 == inByte || 0x35 == inByte || 0x36 == inByte || 0x37 == inByte || 0x38 == inByte))
    {
      switch(inByte)
      {
        case 0x03:
        case 0x33:
        
        requestStage = 0x00;
        break;
        
        case 0x05:
        case 0x35:
        reqLedOn();
        requestStage = 0x00;
        break;
        
        case 0x06:
        case 0x36:
        reqLedOff();
        requestStage = 0x00;
        break;
        
        case 0x07:
        case 0x37:
        for(int i = 0; i < recLogCounter; i++)
        {
          while(!(UCSR0A & (1<<UDRE0)))
          {
          }
          UDR0 = recLog[i];
        }
        break;

        case 0x08:
        case 0x38:
        for(int i = 0; i < recLogSize; i++)
        {
	        recLog[i] = 0x00;
			recLogCounter = 0x00;
        }
        break;
        
        default:
        {
        }
      }
    }
    else
    {
      if((0x00 == requestStage)
         && (0x01 == inByte || 0x02 == inByte || 0x04 == inByte 
		  || 0x31 == inByte || 0x32 == inByte || 0x34 == inByte))
      {
        requestStage = inByte;
      }
      else
      {
        if(0x00 != inByte)
        {
          switch(requestStage)
          {
          case 0x01:
          case 0x31:
            
            requestStage = 0x00;
          break;

          case 0x02:
          case 0x32:
            
            requestStage = 0x00;
          break;
        
          case 0x04:
          case 0x34:
          reqLedToggle(inByte);
          requestStage = 0x00;
          break;
              
          default:
            {
            }
          }
        }
      }
    }
  }
}

