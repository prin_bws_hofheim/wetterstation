/*
   File:   newmain.c
   Author: thomas

   Created on 2. März 2023, 17:51
*/

#include <stdio.h>
//#include <cstdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#ifndef F_CPU
/* Definiere F_CPU, wenn F_CPU nicht bereits vorher definiert
   (z.B. durch Übergabe als Parameter zum Compiler innerhalb
   des Makefiles). Zusätzlich Ausgabe einer Warnung, die auf die
   "nachträgliche" Definition hinweist */
#define F_CPU 16000000UL     /* Quarz mit 16 Mhz */
#endif
#include <util/delay.h>
void init() {
  DDRB |= ((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3));
  DDRC |= ((1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3) | (1 << PC4) | (1 << PC5));
  DDRD |= ((1 << PD2) | (1 << PD3) | (1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7));

  PORTB |= ((1 << PB4) | (1 << PB5));

  //timer 1
  //    TIMSK1 |= (1 << TOIE1);

  //    sei();
}

void countDice() {

  PORTD &= ~(1 << PD2);

  PORTD |= (1 << PD2);
  
  PORTD &= ~(1 << PD3);

  PORTD |= (1 << PD3);
  
  PORTD &= ~(1 << PD4);

  PORTD |= (1 << PD4);
  
  PORTD &= ~(1 << PD5);

  PORTD |= (1 << PD5);
  
  PORTD &= ~(1 << PD6);

  PORTD |= (1 << PD6);
  
  PORTD &= ~(1 << PD7);

  PORTD |= (1 << PD7);

  
  PORTB &= ~(1 << PB0);

  PORTB |= (1 << PB0);
  
  PORTB &= ~(1 << PB1);

  PORTB |= (1 << PB1);
  
  PORTB &= ~(1 << PB2);

  PORTB |= (1 << PB2);
  
  PORTB &= ~(1 << PB3);

  PORTB |= (1 << PB3);
}
void countTraficLights() {
  PORTC &= ~(1 << PC0);

  PORTC |= (1 << PC0);

  PORTC &= ~(1 << PC1);

  PORTC |= (1 << PC1);

  PORTC &= ~(1 << PC2);

  PORTC |= (1 << PC2);

  PORTC &= ~(1 << PC3);

  PORTC |= (1 << PC3);

  PORTC &= ~(1 << PC4);

  PORTC |= (1 << PC4);

  PORTC &= ~(1 << PC5);

  PORTC |= (1 << PC5);  
}

/*

*/
int main(int argc, char** argv)
{
  init();
  while (1)
  {
    countDice();
    countTraficLights();
  }
  return (0);
}

