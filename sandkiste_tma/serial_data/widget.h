#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    enum sendModeType{sendModeBin, sendModeAscii, sendModeNone = -1};

private slots:
    void on_btnListComPorts_clicked();
    void on_btnOpenComPort_clicked();
    void on_btnSendData_clicked();
    void on_btnReadData_clicked();
    void on_btnLedOn_clicked();
    void on_btnLedOff_clicked();
    void on_btnClearBuffer_clicked();
    void on_btnSendLog_clicked();
    void on_btnClearLog_clicked();

    void on_btnBinMode_clicked();

    void on_btnAsciiMode_clicked();

private:
    Ui::Widget *ui;

    QSerialPort serial;
    QList<QSerialPortInfo> portList;
//    QByteArray byteArray;
    sendModeType sendMode;

    void btnToggleLed();
};
#endif // WIDGET_H
