#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->btnSendData->setEnabled(false);
    ui->btnReadData->setEnabled(false);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_btnListComPorts_clicked()
{
    portList = QSerialPortInfo::availablePorts();

    ui->listSerialPorts->clear();
    for(int i = 0; i < portList.size(); i++)
    {
        ui->listSerialPorts->addItem(QString::number(i) + " " + portList[i].portName() + " " + portList[i].description());
    }
    ui->btnOpenComPort->setEnabled(true);
}


void Widget::on_btnOpenComPort_clicked()
{
    if(!portList.isEmpty())
    {
        serial.setPort(portList[ui->listSerialPorts->currentRow()]);//ui->spinSelectedPort->value()
        serial.setFlowControl(QSerialPort::NoFlowControl);
        serial.setParity(QSerialPort::NoParity);

        connect(&serial, &QIODevice::readyRead, this, &Widget::on_btnReadData_clicked);

        serial.open(QIODevice::ReadWrite);
        if(serial.isOpen())
        {
            ui->edtLog->appendPlainText("port open");

            serial.setDataTerminalReady(true);
            ui->btnAsciiMode->setEnabled(true);
            ui->btnBinMode->setEnabled(true);
        }
        else
            ui->edtLog->appendPlainText("port error");
    }
}


void Widget::on_btnSendData_clicked()
{
    QString sendString;
    sendString = ui->edtSendText->text();

    QDataStream dataStream(&serial);
    char sendChar = 0x00;

    switch (sendMode) {
    case sendModeBin:
        for(int i = 0; i<sendString.size(); i++)
        {
            sendChar = sendString[i].toLatin1();
            qDebug() <<  sendChar;
            dataStream << (quint8)sendChar;
        }
        break;
    case sendModeAscii:
        for(int i = 0; i<sendString.size(); i++)
        {
            sendChar = sendString.toShort();
            dataStream << (quint8)sendChar;
        }
        break;
    default:
        ;
    }
}


void Widget::on_btnReadData_clicked()
{
    QByteArray recByteArray;
    QString outString;

    if(serial.bytesAvailable())
    {
        recByteArray = serial.readAll();
        for(int i = 0; i < recByteArray.size(); i++)
            outString.append(QString::number(recByteArray[i], 16) + " ");
        ui->edtLog->appendPlainText(outString);
    }
}


void Widget::btnToggleLed()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    switch (sendMode) {
    case sendModeBin:
        sendChar = 0x04;
        break;
    case sendModeAscii:
        sendChar = 0x34;
        break;
    default:
        ;
    }
    dataStream << (quint8)sendChar;

    sendChar = 0x35;
    dataStream << (quint8)sendChar;
}


void Widget::on_btnLedOn_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    switch (sendMode) {
    case sendModeBin:
        sendChar = 0x05;
        break;
    case sendModeAscii:
        sendChar = 0x35;
        break;
    default:
        ;
    }
    dataStream << (quint8)sendChar;}


void Widget::on_btnLedOff_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    switch (sendMode) {
    case sendModeBin:
        sendChar = 0x06;
        break;
    case sendModeAscii:
        sendChar = 0x36;
        break;
    default:
        ;
    }
    dataStream << (quint8)sendChar;
}


void Widget::on_btnClearBuffer_clicked()
{
    ui->edtLog->clear();
}


void Widget::on_btnSendLog_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    switch (sendMode) {
    case sendModeBin:
        sendChar = 0x07;
        break;
    case sendModeAscii:
        sendChar = 0x37;
        break;
    default:
        ;
    }
    dataStream << (quint8)sendChar;
}


void Widget::on_btnClearLog_clicked()
{
    char sendChar;
    QDataStream dataStream(&serial);

    sendChar = 0x08;
    dataStream << (quint8)sendChar;
}


void Widget::on_btnBinMode_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    ui->btnSendData->setEnabled(true);
    ui->btnReadData->setEnabled(true);
    ui->btnLedOn->setEnabled(true);
    ui->btnLedOff->setEnabled(true);
    ui->edtSendText->setEnabled(true);
    ui->btnClearLog->setEnabled(true);
    ui->btnClearBuffer->setEnabled(true);
    ui->btnSendLog->setEnabled(true);
    ui->listSerialPorts->setEnabled(false);
    ui->btnOpenComPort->setEnabled(false);
    ui->btnAsciiMode->setEnabled(false);
    ui->btnBinMode->setEnabled(false);
    sendMode = sendModeBin;

    sendChar = 0x00;
    dataStream << (quint8)sendChar;
}


void Widget::on_btnAsciiMode_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(&serial);

    ui->btnSendData->setEnabled(true);
    ui->btnReadData->setEnabled(true);
    ui->btnLedOn->setEnabled(true);
    ui->btnLedOff->setEnabled(true);
    ui->edtSendText->setEnabled(true);
    ui->btnClearLog->setEnabled(true);
    ui->btnClearBuffer->setEnabled(true);
    ui->btnSendLog->setEnabled(true);
    ui->listSerialPorts->setEnabled(false);
    ui->btnOpenComPort->setEnabled(false);
    ui->btnAsciiMode->setEnabled(false);
    ui->btnBinMode->setEnabled(false);
    sendMode = sendModeAscii;

    sendChar = 0x30;
    dataStream << (quint8)sendChar;
}

