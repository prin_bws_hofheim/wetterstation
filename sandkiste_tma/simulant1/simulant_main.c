/* 
 * File:   simulant_main.c
 * Author: thomas
 *
 * Created on 25. Februar 2022, 18:15
 */

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>

void initUart(){
    
    UBRR0H = 0x00;
    UBRR0L = 0x67;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0); // | (1 << TXCIE0);
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // 8bit
    //init LED
    DDRB |= (1 << PB5);
    PORTB |= (1 << PB5);
}

/*
 * 
 */
int main(int argc, char** argv) {
    unsigned char inByte = 0x00;
    initUart();
    
    char recString[10];
    
    for(;;){
    
        while(!(UCSR0A & (1 << RXC0)))
        ;
        while (!(UCSR0A & (1<<UDRE0)))
		;
		inByte = UDR0;
    if(0 == strncmp(inByte, "hello",5)){
        
    }
    }
    return (EXIT_SUCCESS);
}

