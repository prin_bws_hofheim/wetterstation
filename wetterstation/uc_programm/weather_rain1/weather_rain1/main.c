/*
 * weather_rain1.c
 *
 * Created: 30.05.2021 11:44:59
 * Author : Thomas
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

volatile int counter0 = 0x00;

int main()
{
	DDRB |= 0x20;
	DDRD |= 0x00;
	TCCR0A = 0x00;
	TCCR0B = 0x00; //CS02=1, CS01= 1; CS00 = 1;Clock on rising edge.
	TCNT0 = 0xF5;
	TIMSK0 |= 0x01;
	int windRainSelector = 0x00;	

	sei();

// Auswahl: ir Reflex (Wind) oder Int T0 (Rain)
// gedr�ckt == ir Reflex
    if(PIND &0x10)
	    windRainSelector = 0x01;
		
    TCCR0B = 0x07; //CS02=1, CS01= 1; CS00 = 1;Clock on rising edge.

	while(1)
	{
		if(0x01 == windRainSelector)
		{
			if(1 <= counter0)
			{
				if(PINB & 0x20)//PIN B5
				{
					PORTB &=0xDF;
					counter0 = 0x00;
				}
				else
				{
					PORTB |= 0x20;
					counter0 = 0x00;
				}
				TCNT0 = 0xF5;
			}
		}
		else
		{
			if(PIND & 0x20)//PIN D5
			{
				PORTB &=0xDF;
			}
			else
			{
				PORTB |= 0x20;
			}
		}
	}
	return 0;
}


ISR( TIMER0_OVF_vect )
{
	counter0++;	
}