void setup() {
  // put your setup code here, to run once:
  unsigned char i;
  /* Define pull-ups and set outputs high */
  /* Define directions for port pins */
  PORTB = (1<<PB7)|(1<<PB6)|(1<<PB1)|(1<<PB0);
  DDRB = (1<<DDB3)|(1<<DDB2)|(1<<DDB1)|(1<<DDB0);
  
  PORTD = (0<<PD7)&(0<<PD6)&(0<<PD3);
  DDRD  = (0<<DDD7)&(0<<DDD6)&(0<<DDD3);
  /* Insert nop for synchronization*/
}

void loop() {
  // put your main code here, to run repeatedly:

}
