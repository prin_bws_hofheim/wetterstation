#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->btnSendData->setEnabled(false);
    ui->btnReadData->setEnabled(false);
    serial = new QSerialPort(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::closeEvent(QCloseEvent *event)
{
    serial->close();
    event->accept();
}

void Widget::on_btnListComPorts_clicked()
{
    portList = QSerialPortInfo::availablePorts();

    ui->listSerialPorts->clear();
    for(int i = 0; i < portList.size(); i++)
    {
        ui->listSerialPorts->addItem(QString::number(i) + " " + portList[i].portName() + " " + portList[i].description());
    }
    ui->btnOpenComPort->setEnabled(true);
}


void Widget::on_btnOpenComPort_clicked()
{
    if(!portList.isEmpty())
    {
        serial->setPort(portList[ui->listSerialPorts->currentRow()]);//ui->spinSelectedPort->value()
        serial->setFlowControl(QSerialPort::NoFlowControl);
        serial->setParity(QSerialPort::NoParity);

        serial->open(QIODevice::ReadWrite);
        if(serial->isOpen())
        {
            ui->edtLog->appendPlainText("port open");

            serial->setDataTerminalReady(true);
            QObject::connect(serial, &QSerialPort::readyRead, this, &Widget::readData);

            ui->btnSendData->setEnabled(true);
            ui->btnReadData->setEnabled(true);
            ui->edtSendText->setEnabled(true);
            ui->btnClearLog->setEnabled(true);
            ui->btnClearBuffer->setEnabled(true);
            ui->btnSendLog->setEnabled(true);
            ui->listSerialPorts->setEnabled(false);
            ui->btnOpenComPort->setEnabled(false);
            ui->edtSendText->setFocus();
        }
        else
            ui->edtLog->appendPlainText("port error");
    }
}


void Widget::on_btnSendData_clicked()
{
    sendData();
}

void Widget::sendData()
{
    QString sendString;
    QStringList sendStringList;
    QString tempString;
    sendString = ui->edtSendText->text();
    sendStringList= sendString.split(";");

    QDataStream dataStream(serial);
    char sendChar = 0x00;

    for(int i = 0; i<sendStringList.size(); i++)
    {
        sendChar = sendStringList[i].toInt(nullptr, 16);
        dataStream << (quint8)sendChar;
    }
    if(ui->checkAutoClean->isChecked())
        ui->edtLog->clear();
}


void Widget::on_btnReadData_clicked()
{
    readData();
}

void Widget::on_btnClearBuffer_clicked()
{
    ui->edtLog->clear();
}


void Widget::on_btnSendLog_clicked()
{
    char sendChar = 0x00;
    QDataStream dataStream(serial);

    sendChar = 0x07;
    dataStream << (quint8)sendChar;
}


void Widget::on_btnClearLog_clicked()
{
    char sendChar;
    QDataStream dataStream(serial);

    sendChar = 0x08;
    dataStream << (quint8)sendChar;
}


void Widget::readData()
{
    QByteArray recByteArray;
    QString outString;

    if(serial->bytesAvailable())
    {
        recByteArray = serial->readAll();
        for(int i = 0; i < recByteArray.size(); i++)
            outString.append(QString::number(recByteArray[i], 16) + " ");
        ui->edtLog->appendPlainText(outString);
    }
}

void Widget::on_edtSendText_returnPressed()
{
    sendData();
}

