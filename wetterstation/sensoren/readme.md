# Projekt Wetterstation 
## Teilprojekt Sensoren 
Mechanik, Bauteile für Messung von

- Temperatur
- Luftfeuchtigkeit
- Windrichtung
- Windgeschwindigkeit
- Regenmenge


## Mögliche Sensoren

| Messung | SensorTyp | Sensor | Pins |
| --- | --- | --- | --- |
| Regen | Hall | AH3661UA | ?? |
| Windgeschindigikeit | Reflexlichtschranke (1 - 2 Stück) | SG128IR | ?? |
| Windrichtung | Reflexlichtschranke (4 Stück) | SG128IR | ?? |
| Temperatur, Luftfeuchtigkeit | Kombisensor | dht22 | ?? |
| Sonne | Fototransistor | pt331 | ?? |
 
