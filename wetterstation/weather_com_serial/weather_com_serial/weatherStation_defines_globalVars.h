#ifndef WEATHERSTATION_GLOBALVARS_H
#define WEATHERSTATION_GLOBALVARS_H

#include <avr/io.h>
#define arraySize 100

unsigned char recLog[arraySize];
unsigned char recLogCounter;

unsigned char req01Data[arraySize];
unsigned char req01Counter;
unsigned int req01SizeIn;

#endif // WEATHERSTATION_GLOBALVARS_H