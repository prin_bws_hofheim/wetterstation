/*
* weather_com_serial.c
*
* Created: 18.09.2021 15:20:04
* Author : Thomas
*/

#include "weatherStation_defines_globalVars.h"
//#include "weather_com_serial_bin.cpp"
//#include "../temp_hum_dht22.cpp"
#include <avr/pgmspace.h>

const int16_t humTempArraSize = 40;

const int16_t humTempArray[] =
{
	6300,2630,
	6070,2630,
	6220,2630,
	6400,2630,
	6350,2640,
	6260,2640,
	6080,2640,
	5880,2640,
	5900,2640,
	5840,2640,
	5760,2640,
	5730,2640,
	5710,2640,
	5660,2640,
	5630,2640,
	5690,2640,
	5670,2640,
	5750,2640,
	5690,2640,
	5750,2640
};

void processProtocollBin();
void reqProtocolType();
void sendAllRecords();
void sendRecord(int16_t recordNr);
void sendChannelData(unsigned char channel, int16_t data);
void sendDataSize(unsigned char channelCount);
void sendBinData(unsigned char sendData);

void initSerialCom()
{
	//init UART
	UBRR0H = 0x00;
	UBRR0L = 0x67;
	UCSR0B = 0x18;
	UCSR0C = 0x06;

	DDRB |= 0x20;
	PORTB |= 0x20;
	
	for(int recLogCounter = 0; recLogCounter < arraySize; recLogCounter++)
	{
		recLog[recLogCounter] = 0x00;
		req01Data[recLogCounter] = 0x00;
	}
	recLogCounter = 0x00;
	req01Counter = 0x00;
	req01SizeIn = 0x00;
}

int main(void)
{
	char swichOn = 0x00;
	unsigned char inByte = 0x00;
	
	initSerialCom();

	while (1)
	{
		while(!(UCSR0A & 0x80))
		{
			if(~PINB & 0x80)
			{
				if(!swichOn)
				{
					if(PORTB & 0x20)
					PORTB &=0xDF;
					else
					PORTB |=0x20;
					
					UDR0 = 0x33;
					swichOn = 1;
				}
			}
			else
			swichOn = 0;
		}
		while (!(UCSR0A & 0x20))
		;
		inByte = UDR0;
		recLog[recLogCounter] = inByte;
		recLogCounter++;

		switch(inByte)
		{
			case 0x01:
			reqProtocolType();
			break;
			
			case 0x11:
			sendAllRecords();
			break;

			case 0x12:
			sendRecord(2);
			break;
		}
	}
}


void reqProtocolType()
{
	sendBinData(0x01);
	sendBinData(0x10);
	sendBinData(0x01);
}

void sendAllRecords()
{
	unsigned char channelLow = 0;
	unsigned char channelHigh = 0;

	sendBinData(0x11);
	
	channelLow = humTempArraSize * 3 % 0x100;
	channelHigh = humTempArraSize * 3 / 0x100;
	sendBinData(channelHigh);
	sendBinData(channelLow);

	for(int16_t i = 0; i< humTempArraSize; i+=2)
	{
		sendBinData(i);
		channelLow =humTempArray[i] % 0x100;
		channelHigh =humTempArray[i] / 0x100;
		sendBinData(channelHigh);
		sendBinData(channelLow);
	}
}

void sendRecord(int16_t pairNr)
{
	unsigned char dataSize = 0;
	int16_t sendData = 0;
	
	pairNr *=2;
	if(pairNr>humTempArraSize-1)
	return;
	
	sendBinData(0x12);
	dataSize = 6;
	sendBinData(dataSize);
	
	sendData = humTempArray[pairNr];
	sendChannelData(pairNr, sendData);
	pairNr++;
	sendData = humTempArray[pairNr];
	sendChannelData(pairNr, sendData);
}

void sendChannelData(unsigned char channelNr, int data)
{
	unsigned char channelLow = 0;
	unsigned char channelHigh = 0;
	
	sendBinData(channelNr);
	
	channelLow = data % 0x100;
	channelHigh = data / 0x100;
	sendBinData(channelHigh);
	sendBinData(channelLow);	
}

void sendBinData(unsigned char sendData)
{
	while (!(UCSR0A & 0x20))
	;
	UDR0 = sendData;
}
