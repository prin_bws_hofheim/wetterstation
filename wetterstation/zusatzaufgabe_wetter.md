# Zusatzaufgabe 1 - Arduino

Thema: Erstelle einen Client mit Arduino (z.B. Wetterstation, Parkhaus, …) Der Client soll per RS232 mit dem PC kommunizieren. Das Protokoll ist hier vorgegeben.
https://bws.mtk.hessencampus.studiumdigitale.uni-frankfurt.de/moodle/mod/resource/view.php?id=98579
Verwende Kommunikation mit Prüfbits (Befehlssatz 0x2n).

Kanal 0x41,
Berechnung der Prüfbits: gerade Stelle im Byte mit 3, ungerade Stelle im Byte mit 2. gilt für Befehl, Kanal, Daten. Alle Produkte werden addiert und modulo 0xFF gerechnet.
Das Ergebnisse aus der Modulodivision wird an die Daten angehängt. Die Gesamtlänge ist dann Länge Kanal + Länge Daten + Länge Prüfbyte (4).
## Beispiel

Befehl 0x20, Kanal 0x41, Wert 0x00 0x19

Prüfsumme: 0x33

Übertragung: 0x20 0x04 0x41 0x00 0x19 0x33
Geforderte Abgabe:

    Klassendiagramm
    Quellcode mit Blockkommentaren vor den Methoden (außer getter und Setter)
    Ggf kurze Userdoku (falls das Programm nicht selbsterklärend ist)

## Stufe 1

Einzelarbeit, 1Bonuspunkt gesamt

Simuliere mit einem Taster (z.B. eingebauter bei explained-Platine) einen Regensensor für eine Wetterstation.
Der Controller sendet für jeden Tastendruck eine Nachricht. Der PC sammelt die Ereignisse (Tastendrücke) über den Zeitraum einer Minute, summiert sie und speichert die Summe alle Minute ab (List oder Vector). Die Anzeige am PC erfolgt lokal und auf Abruf des Users (Konsole oder GUI)
## Stufe 2

Einzelarbeit, 2 Bonuspunkte oder
Partnerarbeit je 1 Bonuspunkt

Wie Stufe 1, zusätzlich gibt es eine Anzeige via Netzwerkverbindung (PC zu PC). Realisierung als zwei Programme am PC oder ein Programm, dass mit dem Controller kommuniziert und im Netzwerk Client und Server ist.
## Stufe 3

Partnerarbeit, 2 Bonuspunkte

Wie Stufe 1+2. Änderung am Controller: Der Controller sammelt über je eine Minute die Ereignisse (Tastendruck) und sendet die Summe dann an den PC.

Der PC hat eine lokale Anzeige (Konsole oder GUI) und eine Anzeige via Netzwerk (vgl Stufe 2).
