# Projekt Wetterstation 
Hauptverzeichnis 
## Teilprojekte (Verzeichnisse) sind:
* sensoren (Mechanik, Bauteile)
* adapter_elektronik (Platine für Arduino uno)
* controller_platine  (noch leer, später eine Platine statt aufsatz zu arduino uno)
* uc_programm (Programm für den Controller (atmega328 / ...)
* pc_programm (Programm für den PC (Anbindung Controller, ggf Anbindung Clients)

## Systemaufbau
|Gehäuse Nr|Inhalt|
|:---|:---|
|1|arduino, Helligkeit, 
|1a|Temperatur (unten anbauen an Nr 1, Gehäuseteil ist offen (Kühlrippen)|
|2|Windgeschwindigkeit|
|3|Windrichtung|
|4|Regensensor|
