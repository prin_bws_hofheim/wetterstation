# Projekt Wetterstation 
## Teilprojekt Adapter/Elektronik 
Platine für Arduino unoUmsetzung der Signale der Senosren, 

ggf Schmitt Trigger, ad Wandler, SPI für mehr pins, ...

| Sensor   | Pin Ausgang | Anzahl Pins | uC (Atmega 328) | Pin | Inhalt |
|---       |---          |---          |---              |---  |---     |
| dht22    | 2           | 4           |                 | D2  | Temperatur, Luftfeuchtigkeit |
| AH3661UA | 3           | 3           |                 | D3  | Regensensor, Wippe |
| SG128IR  | 3           | 4           |                 | D4  | ir Lichtschranke (Windgeschwindigkeit 1) |
| SG128IR  | 3           | 4           |                 | D5  | ir Lichtschranke (Windgeschwindigkeit 2) |
| SG128IR  | 3           | 4           |                 | D6  | ir Lichtschranke (Windrichtung 1) |
| SG128IR  | 3           | 4           |                 | D7  | ir Lichtschranke (Windrichtung 2) |
| SG128IR  | 3           | 4           |                 | B0  | ir Lichtschranke (Windrichtung 3) |
| SG128IR  | 3           | 4           |                 | B1  | ir Lichtschranke (Windrichtung 4) |
| pt331    | 2           | 2           |                 | C0  | Helligkeit (Sonne/Tag-Nacht) |

## Adapter
| IC      | Funktion                     | Anmerkung |
| ---     | ---                          | --- |
| 74HC132 | analog nach digital umsetzer | zu ir Lichtschranke; mit Inverter! |

