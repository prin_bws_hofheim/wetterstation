# Regeln fürs Design - Hardware

Die Wetterstation soll aus mehreren Modulen bestehen. Ein Modul koppelt die Sensoren mit einem PC (oder zeichnet offline die Werte selbst auf).

Es sollen später
- Windrichtung
- Wind Geschwindigkeit
- Regenmenge (pro Stunde, ...)
- Beleuchtungsstärke (Helligkeit)
gemessen werden.

Als Basis des Hauptmoduls dient ein Arduino Uno R3. Grundsätzlich benötigen wir für standalone nur den zentralen IC (ATMEGA 328).
Der Anschluss der Sensoren erfolgt über Leitungen. Zum teil verlaufen die Leitungen von einer Box zu einer anderen. Die Durchgänge (Wanddurchbrüche) müssen für den Einsatz außer Haus (bei Wind- und Regenmessung sinnvoll) wetterfest sein. IP44 oder besser ist anzustreben.

Die Verbindung der Sensoren mit dem Arduino kann grundsätzlich durch Einstecken der Drähte in die Anschlüsse der Arduino-Platine erfolgen. Dies ist jedoch wenig stabil. Stattdessen soll eine Aufsatzplatine (Shield) den Kontakt herstellen.

## Welche Sensoren für was?

Als Helligkeitssensor werden wir einen LDR (lichtempfindlichen Widerstand) oder einen Fototransistor/ Fotodiode verwenden. Um unterscheiden zu können, wie hell es ist, benötigen wir einen analogen Eingang. Bei der Schaltung beachten, dass der Sensor ggf. eine maximale Spannung und einen maximalen Strom verträgt. P<sub>tot</sub> bedeutet eigentlich die maximale Leistung, die das Bauteil verträgt, wird gerne auch als "wann ist das Ding tot?" bezeichnet. Diese Leistung darf nicht (auch nicht kurz) überschritten werden. Besser Vorwiderstand einbauen, der die Spannung bzw. den Strom sicher darunter hält.

Für die Windrichtung können wir eine vier stufige Skala verwenden (Auflösung 22,5°) -> Eingang digital.

Für die Windgeschwindigkeit entweder einen kleinen Motor (Anschluss analog) oder einen Impulsgeber (Magnet an der Achse, Scheibe mit Farbmarkierung) -> Eingang digital.

Für den Regen können wir einen Kipplöffel oder eine zweiseitige Wippe verwenden, beide liefern über einen Magneten Impulse. -> Eingang digital.

## Welche Pins können was?

Grundsätzlich bietet der Arduino zwei Arten Anschlüsse:
- analog (A0 - A5)
- digital (0 - 13)

Die analogen Pins lassen sich auch als digitale Pins nutzen.

Der Helligkeitssensor bekommt einen Analogpin. 

Bei den anderen Sensoren ist es fast gleich, an welche Pins sie angeschlossen werden.

Pin 0 und 1 sollten wir nicht verwenden, daran ist die serielle Schnittstelle angeschlossen und damit der PC (vis USB-IC).

Pin 2 und 3 haben im Schaltplan des Arduinos den Zusatz INTn. Hardware Interrupts unterbrechen das Programm und lassen wichtige Aktionen *jetzt* ausführen.

Pin 4 und 5 haben den Zusatz Tn. Mit ihnen lassen sich Timer und Zähler beeinflussen. 

Die Übrigen Pins haben ebenfalls Zusatzfunktionen (Siehe Datenblatt des Herstellers).

## Welche Pins verwenden?

Eigentlich ist es also gleich, welche digitalen Sensoren ich wo anschließe. Wenn ich meinen Programmierer ärgern will, wähle ich:

| Pin | Sensor |
| --- | ---    |
| 2   | Windrichtung 1 |
| 3   | Windrichtung 2 |
| 4   | Windrichtung 3 |
| 5   | Windrichtung 4 |
| 6   | Windgeschwindigkeit |
| 7   | Regen |

Mit dem Layout kann er nur polling machen. Ständig abfragen, welchen Wert (0 oder 1) der Pin hat.

Besser wäre es Pins 4 oder 5 für Regen zu nutzen. Pin 2 oder 3 wäre auch möglich.

Bei der Windgeschwindigkeit gilt das Gleiche. Mit dem Zähler kann ich die Ereignisse pro Zeit (z.B. pro Minute, Stunde, ...) messen.

Die Windrichtung kann ich gut per Polling abfragen. Mit dem 4bit Wert kann ich ständig die Windrichtung bestimmen. Ich muss nicht wissen wie sich die Fahne zuletzt gedreht hat. Lediglich Spielkinder, die die Windfahne als Kreise benutzen kann ich so nicht "abfangen". Bei passender Montage der Wetterstation (auf einem Mast, Dach, ...) sollte das aber unproblematisch sein. Wenn dann die Windfahne noch immer schnell rotiert steht die Station wahrscheinlich in einem Tornado, solang die Befestigung und übrige Mechanik durchhält.
