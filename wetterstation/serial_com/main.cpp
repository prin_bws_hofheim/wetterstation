#include "widgetcommunication.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WidgetCommunication w;
    w.show();
    return a.exec();
}
