#include "dialogconnectcontrollerserial.h"
#include "ui_dialogconnectcontrollerserial.h"

DialogConnectControllerSerial::DialogConnectControllerSerial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogConnectControllerSerial){
    selectedPortIndex = -1;

    ui->setupUi(this);
    connect(ui->btnConnect, &QPushButton::clicked, this, &DialogConnectControllerSerial::selectComPort);
    connect(ui->btnRefreshList, &QPushButton::clicked, this, &DialogConnectControllerSerial::listComPorts);
    listComPorts();
}

DialogConnectControllerSerial::~DialogConnectControllerSerial(){
    delete ui;
}

void DialogConnectControllerSerial::listComPorts(){
    comPorts.clear();
    comPorts = QSerialPortInfo::availablePorts();
    ui->listWComPorts->clear();
    for(int i = 0; i < comPorts.size(); i++)    {
        ui->listWComPorts->addItem(QString::number(i) + " " + comPorts[i].portName() + " " + comPorts[i].description());
    }
}

void DialogConnectControllerSerial::selectComPort(){
    selectedPortIndex = ui->listWComPorts->currentRow();
}

int DialogConnectControllerSerial::getSelectedPortIndex() const{
    return selectedPortIndex;
}

QSerialPortInfo DialogConnectControllerSerial::getSerialPortInfoEntry(int index){
    if((0 <= index) && (comPorts.size() < index - 1))
        return comPorts.at(index);

    QSerialPortInfo emptyPortInfo;
    return emptyPortInfo;
}
