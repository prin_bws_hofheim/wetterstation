#ifndef WIDGETCOMMUNICATION_H
#define WIDGETCOMMUNICATION_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>

#include "dialogconnectcontrollerserial.h"

QT_BEGIN_NAMESPACE
namespace Ui { class WidgetCommunication; }
QT_END_NAMESPACE

class WidgetCommunication : public QWidget
{
    Q_OBJECT

public:
    WidgetCommunication(QWidget *parent = nullptr);
    ~WidgetCommunication();
private slots:
    void showDialogConnect();

    void recDialogCode(int result);
    void connectController();
private:
    Ui::WidgetCommunication *ui;
    DialogConnectControllerSerial *dialogConnect;

    QSerialPort serialPort;
};
#endif // WIDGETCOMMUNICATION_H
