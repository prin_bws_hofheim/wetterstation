#ifndef DIALOGCONNECTCONTROLLERSERIAL_H
#define DIALOGCONNECTCONTROLLERSERIAL_H

#include <QDialog>
#include <QSerialPortInfo>

namespace Ui {
class DialogConnectControllerSerial;
}

class DialogConnectControllerSerial : public QDialog
{
    Q_OBJECT

public:
    explicit DialogConnectControllerSerial(QWidget *parent = nullptr);
    ~DialogConnectControllerSerial();

    int getSelectedPortIndex() const;

    QSerialPortInfo getSerialPortInfoEntry(int index);
private slots:
    void listComPorts();
    void selectComPort();

private:
    Ui::DialogConnectControllerSerial *ui;
    QList<QSerialPortInfo> comPorts;
    int selectedPortIndex;
};

#endif // DIALOGCONNECTCONTROLLERSERIAL_H
