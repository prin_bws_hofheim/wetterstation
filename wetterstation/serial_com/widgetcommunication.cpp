#include "widgetcommunication.h"
#include "ui_widgetcommunication.h"

WidgetCommunication::WidgetCommunication(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WidgetCommunication){
    ui->setupUi(this);

    dialogConnect = new DialogConnectControllerSerial;

    connect(ui->btnConnectControllerSeralPort, &QPushButton::clicked, this, &WidgetCommunication::showDialogConnect);
    connect(dialogConnect, &DialogConnectControllerSerial::finished, this, &WidgetCommunication::recDialogCode);
}

WidgetCommunication::~WidgetCommunication(){
    delete ui;
    delete dialogConnect;
}

void WidgetCommunication::showDialogConnect(){
    dialogConnect->show();
}

void WidgetCommunication::recDialogCode(int result){

    switch(result){
    case QDialog::Accepted:
        connectController();
        break;

    case QDialog::Rejected:
        break;
    }
}

void WidgetCommunication::connectController(){
    int selectedPortIndex = -1;
    QSerialPortInfo selectedPortInfo;

    selectedPortIndex = dialogConnect->getSelectedPortIndex();
    selectedPortInfo = dialogConnect->getSerialPortInfoEntry(selectedPortIndex);

    serialPort.setPort(selectedPortInfo);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);
    serialPort.setParity(QSerialPort::NoParity);

    serialPort.open(QIODevice::ReadWrite);
    if(serialPort.isOpen()){
        ui->edtLocalLogs->appendPlainText("port open");
        serialPort.setDataTerminalReady(true);
    }
    else
        ui->edtLocalLogs->appendPlainText("port error");
}
