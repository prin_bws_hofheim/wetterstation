#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-Debug.mk)" "nbproject/Makefile-local-Debug.mk"
include nbproject/Makefile-local-Debug.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=Debug
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../main.c /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o

# Source Files
SOURCEFILES=../main.c /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atmega328p"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-Debug.mk dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATmega328P
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/main.o: ../main.c  .generated_files/flags/Debug/21430afd0371761b85da30f5cb4c834d9e7cb9fd .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1472/main.o.d" -MT "${OBJECTDIR}/_ext/1472/main.o.d" -MT ${OBJECTDIR}/_ext/1472/main.o  -o ${OBJECTDIR}/_ext/1472/main.o ../main.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o: /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c  .generated_files/flags/Debug/b626e4b3d2b77b41d1aba3d0643fa6b3dcb2e063 .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1932210834" 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d" -MT "${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d" -MT ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o  -o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o: /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c  .generated_files/flags/Debug/c3358fd112e74e7f71037ccbc3ef84cca01ae899 .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1932210834" 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d" -MT "${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d" -MT ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o  -o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
else
${OBJECTDIR}/_ext/1472/main.o: ../main.c  .generated_files/flags/Debug/617cd4dde072bb3b0e4cd08fbc8ad5347d27e08b .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1472/main.o.d" -MT "${OBJECTDIR}/_ext/1472/main.o.d" -MT ${OBJECTDIR}/_ext/1472/main.o  -o ${OBJECTDIR}/_ext/1472/main.o ../main.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o: /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c  .generated_files/flags/Debug/ec84a5b98ecf0834c1a6912ff8fe91c1efb1106e .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1932210834" 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d" -MT "${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o.d" -MT ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o  -o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_ascii.o /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_ascii.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o: /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c  .generated_files/flags/Debug/2c0f93fec53c2e94a0ab9482afd24ecdddbc77a1 .generated_files/flags/Debug/8fff49d0cb05b727cea9f8f21beb379e9251bb92
	@${MKDIR} "${OBJECTDIR}/_ext/1932210834" 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d 
	@${RM} ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328p ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O0 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DDEBUG -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d" -MT "${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o.d" -MT ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o  -o ${OBJECTDIR}/_ext/1932210834/quasselstrippe_bin.o /home/thomas/schule/material/wetterstation/eigene/wetterstation/quasselstrippe/quasselstrippe/quasselstrippe_bin.c  -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -g2
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega328p ${PACK_COMMON_OPTIONS}   -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,-lm -Wl,--end-group 
	
	${MP_CC_DIR}/avr-objcopy -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.eep" || exit 0
	${MP_CC_DIR}/avr-objdump -h -S "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" > "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.lss"
	${MP_CC_DIR}/avr-objcopy -O srec -R .eeprom -R .fuse -R .lock -R .signature "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.srec"
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega328p ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_Debug=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,-lm -Wl,--end-group 
	${MP_CC_DIR}/avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.hex"
	${MP_CC_DIR}/avr-objcopy -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.eep" || exit 0
	${MP_CC_DIR}/avr-objdump -h -S "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" > "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.lss"
	${MP_CC_DIR}/avr-objcopy -O srec -R .eeprom -R .fuse -R .lock -R .signature "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/quasselstrippe.X.${IMAGE_TYPE}.srec"
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Debug
	${RM} -r dist/Debug

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
