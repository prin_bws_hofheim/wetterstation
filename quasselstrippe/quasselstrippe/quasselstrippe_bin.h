/* 
 * File:   quasselstrippe_bin.h
 * Author: thomas
 *
 * Created on 2. August 2023, 16:15
 */

#ifndef QUASSELSTRIPPE_BIN_H
#define	QUASSELSTRIPPE_BIN_H

void processProtocollBin();
char reqEchoRecBin(unsigned char inByte);
char reqEchoSendBin();
void reqAddBin(unsigned char recValue);
void reqBtnBin(unsigned char recValue);
void reqLedToggleBin(unsigned char recValue);
void reqLedOnBin();
void reqLedOffBin();
void reqSendLogBin();
void reqClearLogBin();
#endif	/* QUASSELSTRIPPE_BIN_H */

