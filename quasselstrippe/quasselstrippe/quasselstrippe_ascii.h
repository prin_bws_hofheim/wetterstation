/* 
 * File:   quasselstrippe_ascii.h
 * Author: thomas
 *
 * Created on 2. August 2023, 16:14
 */

#ifndef QUASSELSTRIPPE_ASCII_H
#define	QUASSELSTRIPPE_ASCII_H

char reqEchoRecAscii(unsigned char inByte);
char reqEchoSendAscii();
void reqAddAscii(unsigned char recValue);
void reqBtnAscii(unsigned char recValue);
void reqLedToggleAscii(unsigned char recValue);
void reqLedOnAscii();
void reqLedOffAscii();
void reqSendLogAscii();
void reqClearLogAscii();
void processProtocollAscii();

#endif	/* QUASSELSTRIPPE_ASCII_H */

