#ifndef __QUASSELSTRIPPE_DEFINES
#define __QUASSELSTRIPPE_DEFINES

#include <avr/io.h>
#define arraySize 100

unsigned char recLog[arraySize];
unsigned char recLogCounter;

unsigned char req01Data[arraySize];
unsigned char req01Counter;
unsigned int req01SizeIn;

#endif // __QUASSELSTRIPPE_DEFINES
