# Programmpaket Quasselstripppe

Das Paket besteht aus einem Programm für den PC und einem Programm für einen Controller (ATMega328).

Als Gesprächspartner für den PC kann man einen Arduino Uno oder Xeplained 328P (Mircochip) verwenden.

## Programmstatus

Das Programm für den Controller ist zur zeit mit dem Explained getestet.

Das Programm für den PC sollen Schüler in einem Projekt selbst implentieren. Das protokoll ist unten beschrieben. Das PC Programm muss nur einen Zweig (binär oder ASCII) implementieren.

Das Programm auf dem Controller kann auf PC-Seite auch mit einem Terminal (z.B. Arduino IDE / Serial Monitor) angesprochen werden.

Hier zuerst 0 eintippen, dann die gewünschten Befehle.

# Protokollbeschreibung zu Quasselstrippe 3

Das Programmpaket "Quasselstrippe" ist zum üben und Verständnis der seriellen Kommunikation für Schüler:innen und andere interessierte entworfen worden.
Es befindet sich zur Zeit noch in der Entwicklung.
Das Programmpaket besteht aus einem Programm für PC (C++/Qt) und einem Programm für einen Microcontroller (Atmega328p) (Atmel, später Microchip) Es ist ein Entwicklungsboard "explained mini 328p" vom Microchip erhältlich. Die Programmiersoftware "Microchip Studio" ist im Internet beim Hersteller kostenlos erhältlich.
Grundsätzlich sollte das Programm auch auf einem Arduino Uno lauffähig sein. Es ist nur auf dem explained mini getestet.

## Ablauf zum Systemtest
 1.	Auf dem PC serial_data starten, Arduino oder Microchip explained 328p mit Programm Quasselstrippe 3 anschließen. Die beiden LEDs müssen dauerhaft leuchten.
 2.	In serial_date Button "list comports" betätigen und Schnittstelle (edbg oder virtual com port oder ähnlich) auswählen.
 3.	Port öffnen (mit open port)
 4.	Auswahl ob binär Übertragung oder ascii Übertragung erfolgen soll.
 5.	Test mit ledOff / ledOn, ob Kommunikation funktioniert.
 6.	Send Log Überträgt die Liste der vom Arduino empfangenen Bytes. Mit readData wird sie angezeigt (kein Autoupdate der empfangenen Daten)
 7.	Das Programm Quasselstrippe (auf dem Arduino) entscheidet mit dem ersten empfangenen Byte, ob es im binär oder ASCII Modus arbeitet. Ein Wechsel ist nur mit Kaltstart (Platine vom USB trennen) möglich.

Das Programmpaket soll Schüler:innen des Fachbereichs Informatik (und andere interessierte) helfen die Kommunikation über die Serielle Schnittstelle zu verstehen. Das Beispiel befasst sich mit einem Protokoll.
Ziel ist, dass die Schüler:innen selbst ein Programm für den PC entwerfen und implementieren, dass mit der Platine (Arduino) kommuniziert.
Zum Beginn sollte ein Test der Funktionalität stehen (Gerät anschließen, LED aus, LED einschalten lassen).
Die Funktion LED aus, LED an ist zu üben der erste Schritt. Wenn dies möglich ist, sollte der Befehl 2 (add) folgen. Danach folgt der Befehl 7 (send log). Die Antwort beinhaltet alle empfangenen Daten. Der Befehl ist auch zur Fehlersuche interessant.

Zum Schluss der Befehl 1 (echo). Hier ist es wichtig, dass die Anzahl der Bytes (Länge er Daten für das Echo) korrekt angegeben werden.

Zum Test kann das Programm serial data verwendet werden. Der Inhalt der Eingabezeile (oben rechts) wird mit "send" an den Controller geschickt.
"read Data" zeigt eine Antwort an. 
Hinweis: nicht alle Befehle senden eine Antwort.
## Protokoll Binär
Alle Befehle werden in hexadezimaler Schreibweise, Byteweise angegeben und übertragen.

Befehl | Sender | Empfänger | Hex-Wert(e) | Inhalt
-------|--------|-----------|-------------|-----------
Init   | PC     | Arduino   | 0x00        | Auswahl des Modus, muss als erstes Zeichen gesendet werden. Wird einmalig in der Kommunikation gesendet. Spätere Werte 0x00 bedeuten inhaltlich 0 (abhängig vom Kontext)
LED an | PC | Arduino | 0x05 | Arduino schaltet die eingebaute LED ein. Die LED ist an Pin B5 angeschlossen
LED aus | PC | Arduino | 0x06 | Arduino schaltet die eingebaute LED aus. Die LED ist an Pin B5 angeschlossen
Echo   | PC | Arduino | 0x01 Länge Inhalt | Länge gibt die Anzahl der nachfolgenden Bytes an. Inhalt den Inhalt der Kommunikation. Der Arduino sendet nach dem Empfang den Inhalt zurück.
Add    | PC | Arduino | 0x02 Wert | Der Arduino empfängt die Zahl, addiert 1 hinzu und sendet sie zurück.
Button | Arduino | PC | 0x03 Wert | Der Arduino sendet den letzten Wert an den PC.
Send Log | PC | Arduino | 0x07 | Antwort: Alle Bytes, die der Controller empfangen hat. Send Log löscht den internen Log nicht
Clear Log | PC | Arduino | 0x08 | Löscht den Log des Controllers.

## Protokoll ASCII
Alle Befehle werden intern hexadezimaler Schreibweise, Byteweise angegeben und übertragen. Die Ein-und Ausgabe erfolgt am Terminal (z.B. Arduino serial Monitor) als Text (0x30 ? '0')

Befehl | Sender | Empfänger | ASCII Zeichen | Inhalt
-------|--------|-----------|---------------|-------
Init | PC | Arduino | 0 (0x30) | Auswahl des Modus, muss als erstes Zeichen gesendet werden. Wird einmalig in der Kommunikation gesendet. Spätere Werte 0x00 bedeuten inhaltlich 0 (abhängig vom Kontext)
LED an | PC | Arduino | 5 (0x35) | Arduino schaltet die eingebaute LED ein. Die LED ist an Pin B5 angeschlossen
LED aus | PC | Arduino | 6 (0x36) | Arduino schaltet die eingebaute LED aus. Die LED ist an Pin B5 angeschlossen
Echo | PC | Arduino | 1(0x31) Länge Inhalt | Länge gibt die Anzahl der nachfolgenden Bytes an. Inhalt den Inhalt der Kommunikation. Der Arduino sendet nach dem Empfang den Inhalt zurück.
Add | PC | Arduino | 2 (0x32) Wert | Der Arduino empfängt die Zahl, addiert 1 hinzu und sendet sie zurück.
Button | Arduino | PC | 3 (0x33) Wert | Der Arduino sendet den letzten Wert an den PC.
Send Log | PC | Arduino | 7 (0x37) | Antwort: Alle Bytes, die der Controller empfangen hat. Send Log löscht den internen Log nicht
Clear Log | PC | Arduino | 8 (0x38) | Löscht den Log des Controllers.
